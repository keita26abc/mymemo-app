module ApplicationHelper
  def full_title
    if @title.present?
      @title + " | " + "Mymemo"
    else
      "Mymemo"
    end
  end
end
